let baseGetUrl = 'https://uxcandy.com/~shapoval/test-task-backend/?developer=Name';

let getTasks = ({ page=1, isAsc=true, field="" } = {}) => {
    return fetch(`${baseGetUrl}&page=${page}&sort_direction=${isAsc ? "asc" : "desc"}&sort_field=${field}`, {
        method: "GET"
    })
        .then(data => data.json());
};
export default {
    getTasks: getTasks
};