import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import './style.css';

export default class Navbar extends Component {
    render() {
        return (
            <header className="main-nav">
                <div className="container">
                    <div className="d-flex justify-content-between">
                        <div>
                            <Link to="/" className="custom-btn text-info"><i className="fas fa-home"></i></Link>
                            <Link to="/newtask" className="btn btn-warning">Создать новую задачу</Link>
                        </div>
                        <div>
                            <Link to="/auth" className="btn btn-outline-success">Войти</Link>
                        </div>
                    </div>
                </div>
            </header>
        );
    }
}
