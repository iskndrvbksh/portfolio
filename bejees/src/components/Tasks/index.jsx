import React, {Component} from 'react';
import apiService from '../../services/mainApiService';

import './Tasks.css';

const TaskTable = (props) => {
    return (
        <table className="table table-bordered task-table">
            <thead>
            <tr>
                <th>
                    <button className="btn btn-link" onClick={e => props.handleSort(e.currentTarget.value)}
                            value="username">Имя<i className={"fas " + (props.sortOptions.username ? "fa-arrow-up" : "fa-arrow-down")}></i>
                    </button>
                </th>
                <th>
                    <button className="btn btn-link" onClick={e => props.handleSort(e.currentTarget.value)}
                            value="email">E-mail<i className={"fas " + (props.sortOptions.email ? "fa-arrow-up" : "fa-arrow-down")}></i>
                    </button>
                </th>
                <th><button className="btn btn-link">Задача</button></th>
                <th>
                    <button className="btn btn-link" onClick={e => props.handleSort(e.currentTarget.value)}
                            value="status">Статус <i className={"fas " + (props.sortOptions.status ? "fa-arrow-up" : "fa-arrow-down")}></i>
                    </button>
                </th>
                <th className="task-table__image-header"><button className="btn btn-link">Изображение</button></th>
            </tr>
            </thead>
            <tbody>
            {props.tasks.map(task => (
                <tr key={task.id}>
                    <td>{task.username}</td>
                    <td>{task.email}</td>
                    <td>{task.text}</td>
                    <td><i className={"fas task-table__icon " + (task.status === 10 ? "text-success fa-check-square" : "text-danger fa-square")}></i>
                    </td>
                    <td><div className="task-table__image-holder"><img className="task-table__image" src={task.image_path} alt=""/></div></td>
                </tr>
            ))}
            </tbody>
        </table>
    );
};


const Pagination = (props) => {
    const pageButtons = [];
    for (let i = 1; i <= props.totalTasks; i++) {
        pageButtons.push(
            <li key={i} className={"page-item " + (i === parseInt(props.activePage, 10) ? "active" : null)}>
                <button onClick={e => props.handlePageChange(i)} className="page-link">{i}</button>
            </li>
        );
    }
    return (
        <nav>
            <ul className="pagination justify-content-center">
                {pageButtons}
            </ul>
        </nav>
    )
};

class Tasks extends Component {
    constructor(props) {
        super(props);
        this.state = {
            taskPerPage: 3,
            tasks: [],
            sort: {
                username: true,
                email: true,
                status: true
            },
            activeField: "",
            activePage: 1
        };
        this.handleSort = this.handleSort.bind(this);
        this.handlePageChange = this.handlePageChange.bind(this);
    }

    handleSort(field) {
        const isAsc = !this.state.sort[field];
        apiService.getTasks({ page: this.state.activePage, field, isAsc}).then(data => {
            this.setState({
                tasks: data.message.tasks,
                totalTasks: Math.round(parseInt(data.message.total_task_count, 10) / this.state.taskPerPage),
                sort: { ...this.state.sort, [field]: !this.state.sort[field] },
                activeField: field
            });
        });
    }

    handlePageChange(page) {
        const options = {
            page,
            field: this.state.activeField ? this.state.activeField : "",
            isAsc: this.state.sort[this.state.activeField]
        };
        apiService.getTasks(options).then(data => {
            this.setState({
                tasks: data.message.tasks,
                totalTasks: Math.round(parseInt(data.message.total_task_count, 10) / this.state.taskPerPage),
                activePage: page
            });
        });
    }

    componentDidMount() {
        apiService.getTasks().then(data => {
            this.setState({
                tasks: data.message.tasks,
                totalTasks: Math.round(data.message.total_task_count / this.state.taskPerPage)
            });
        }).catch(alert);
    }

    render() {
        return (
            <div className="container task-content">
                <TaskTable tasks={this.state.tasks} handleSort={this.handleSort} sortOptions={this.state.sort}/>
                <Pagination activePage={this.state.activePage} totalTasks={this.state.totalTasks} handlePageChange={this.handlePageChange}/>
            </div>
        );
    }
}


export default Tasks;