import React from 'react';

const Pagination = (props) => {
    const pageButtons = [];
    for (let i = 1; i <= props.totalPages; i++) {
        pageButtons.push(
            <li key={i} className={"page-item " + (i === parseInt(props.activePage, 10) ? "active" : null)}>
                <button onClick={props.handlePageChange} value={i} className="page-link">{i}</button>
            </li>
        );
    }
    return (
        <nav>
            <ul className="pagination justify-content-center">
                {pageButtons}
            </ul>
        </nav>
    )
};

export default Pagination;