import React, { Component } from 'react';
import apiService from '../../../services/mainApiService';
import './style.css';
import { IosLoader } from '../../../tools/Loaders';
import Pagination from './Pagination';
import TaskTable from './TaskTable';


class Tasks extends Component {
    constructor(props) {
        super(props);
        this.state = {
            taskPerPage: 3,
            tasks: [],
            sort: {
                username: true,
                email: true,
                status: true
            },
            activeField: "",
            activePage: 1,
            loading: true
        };
    }

    handleSort = (e) => {
        const field = e.currentTarget.value;
        const isAsc = !this.state.sort[field];
        this.showLoader(true);
        apiService.getTasks({ page: this.state.activePage, field, isAsc }).then(data => {
            this.setState({
                tasks: data.message.tasks,
                totalPages: Math.round(parseInt(data.message.total_task_count, 10) / this.state.taskPerPage),
                sort: { ...this.state.sort, [field]: !this.state.sort[field] },
                activeField: field,
                loading: false
            });
        });
    }

    handlePageChange = (e) => {
        const page = e.currentTarget.value;
        const options = {
            page,
            field: this.state.activeField ? this.state.activeField : "",
            isAsc: this.state.sort[this.state.activeField]
        };
        this.showLoader(true);
        apiService.getTasks(options).then(data => {
            this.setState({
                tasks: data.message.tasks,
                totalPages: Math.round(parseInt(data.message.total_task_count, 10) / this.state.taskPerPage),
                activePage: page,
                loading: false
            });
        });
    }

    showLoader = (loading) => {
        this.setState({
            loading
        });
    }

    componentDidMount() {
        this.showLoader(true);
        apiService.getTasks().then(data => {
            this.setState({
                tasks: data.message.tasks,
                totalPages: Math.round(data.message.total_task_count / this.state.taskPerPage)
            }, () => this.showLoader(false));
        }).catch(alert);
    }

    render() {
        const { loading } = this.state
        return (
            <div className="container task-content">
                <div>
                    <TaskTable tasks={this.state.tasks} handleSort={this.handleSort} sortOptions={this.state.sort} />
                    <Pagination activePage={this.state.activePage} totalPages={this.state.totalPages} handlePageChange={this.handlePageChange} />
                </div>
                <IosLoader loading={loading} />
            </div>
        );
    }
}


export default Tasks;