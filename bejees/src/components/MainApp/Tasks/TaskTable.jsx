import React from 'react';

const TaskTable = (props) => {
    const headerButtons = [
        {title: 'Имя', value: "username"},
        {title: "E-mail", value: "email"},
        {title: "Задача", value: "task", sortOff: true},
        {title: "Статус", value: "status"},
        {title: "Изображение", value: "image", sortOff: true}
    ];
    return (
        <table className="table table-bordered task-table">
            <thead>
            <tr>
                {headerButtons.map(button => (
                    <th key={button.value}>
                        <button className="btn btn-link" onClick={!button.sortOff ? props.handleSort : null}
                                value={button.value}>{button.title}{!button.sortOff ? <i className={"fas " + (props.sortOptions[button.value] ? "fa-arrow-up" : "fa-arrow-down")}></i> : null}
                        </button>
                    </th>
                ))}
            </tr>
            </thead>
            <tbody>
            {props.tasks.map(task => (
                <tr key={task.id}>
                    <td>{task.username}</td>
                    <td>{task.email}</td>
                    <td>{task.text}</td>
                    <td><i className={"fas task-table__icon " + (task.status === 10 ? "text-success fa-check-square" : "text-danger fa-square")}></i>
                    </td>
                    <td><div className="task-table__image-holder"><img className="task-table__image" src={task.image_path} alt=""/></div></td>
                </tr>
            ))}
            </tbody>
        </table>
    );
};

export default TaskTable;