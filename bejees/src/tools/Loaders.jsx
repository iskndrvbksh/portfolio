import React from 'react';
import "./style.css";


export class IosLoader extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            loading: nextProps.loading
        };
    }
    render() {
        return (
            <div className="lds-wrapper" style={{ display: this.state.loading ? "block" : "none" }}>
                <div className="lds-spinner"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
            </div>
        );
    }
}

