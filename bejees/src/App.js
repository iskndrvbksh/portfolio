import React, { Component } from 'react';
import firebase from 'firebase';
import { config } from "./config";
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Authentication from "./components/Authentication";
import MainApp from "./components/MainApp";
import NewTask from './components/NewTask';
import NavBar from './components/Navbar';

firebase.initializeApp(config);



class App extends Component {

    componentDidMount() {
        firebase.auth().onAuthStateChanged(user => {
            console.log("user", user)
            if (user) {
                this.setState({ user })
            }
        })
    }

    render() {
        return (
            <BrowserRouter>
            <React.Fragment>
                <NavBar />
                <Switch>
                    <Route exact path="/" component={MainApp} />
                    <Route exact path="/newtask" component={NewTask} />
                    <Route exact path="/auth" component={Authentication} />
                </Switch>
            </React.Fragment>
            </BrowserRouter>
        );
    }
}


export default App;
