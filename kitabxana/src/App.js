import React, { Component } from 'react';
import './App.css';

class App extends Component {
  getData = () => {
    return fetch('/read/screen.json.php/', {
      method: "GET"
    }).then(a => a.json());
  }

  constructor(props) {
    super(props);
    this.state = {
      data:[],
      codes: {},
      config: {
        bottom_config: {},
        modal: {},
        clock: {},
        tasksConfig: {
          0: {},
          1: {},
          3: {}
        }
      },
      date: '01 Yanvar 1971',
      clock: '00:00',
      fulfilled: Array(20).join('X').split(''),
      rejected: Array(20).join('X').split(''),
      isEdit: false,
      time: 5000,
      items: 10
    };
  }

  componentWillUnmount() {
    clearInterval(this.state.timerId);
    clearInterval(this.state.dataTimerId);
  }

  keyHandler = (e) => {
    let dataTimerId = null;
    if (e.keyCode === 116) {
      this.setState(prevState => {
        if (!!prevState.isEdit) {
          clearInterval(this.state.dataTimerId);
          
          dataTimerId = setInterval(() => {
            this.getData().then(this.handler);
          }, this.state.time || 5000);


        }
        return {
          isEdit: !prevState.isEdit,
          dataTimerId: dataTimerId ? dataTimerId : prevState.dataTimerId
        }
      });
    }
  }
  itemsChange = (e) => {
    this.setState({
      items: +e.target.value
    });
  }

  handleTime = (e) => {
    this.setState({
      time: +e.target.value
    });
  }

  handler = (data) => {
    let filtered = {
      data: data.data,
      codes: data.codes,
      config: data.config
    };
    const res = this.storeData(filtered);

    filtered.data = filtered.data.slice(0, this.state.items);
    this.setState(Object.assign({}, filtered, res));


    if (this.state.getDataCalled) {
      if (this.state.lastTask !== filtered.data[0].OXUCU_ID) {
        this.setState({
          lastTask: filtered.data[0].OXUCU_ID,
          modalActive: true
        });

        setTimeout(() => {
          this.setState({
            modalActive: false
          });
        }, filtered.config.modal.second * 1000);
      } else {
        this.setState({
          modalActive: false
        });
      }
    } else {
      this.setState({
        lastTask: filtered.data[0].OXUCU_ID
      });
    }
  }
  componentDidMount() {
    document.body.addEventListener('keypress', this.keyHandler);
    this.generateDate();
    let timerId = setInterval(this.generateDate, 1000);
    
    this.getData().then(this.handler).then(a => {
      this.setState({ getDataCalled: true });
    });
    let dataTimerId = setInterval(() => {
      this.getData().then(this.handler)
    }, this.state.time || 5000);
    


    this.setState({
      timerId,
      dataTimerId,
    });

    
  }

  storeData = (value) => {
    const combine = value.data.slice(20);

    const fulfilled = Array(20).join('X').split('');
    let index = 0;
    combine.forEach((item) => {
      if (item.STATUS === '0' || item.STATUS === '3') {
        fulfilled[index] = item;
        index++;
      }
    });
    return { fulfilled: fulfilled.slice(0, 10), rejected: fulfilled.slice(9) };
  }

  generateDate = () => {
    const time = new Date();
    let year = time.getFullYear();
    let month = time.getMonth();
    let date = time.getDate();
    let hours = time.getHours();
    let minutes = time.getMinutes();
    const months = [
      'Yanvar',
      'Fevral',
      'Mart',
      'Aprel',
      'May',
      'Iyun',
      'Iyul',
      'Avqust',
      'Sentyabr',
      'Oktyabr',
      'Noyabr'
    ];

    month = months[month];

    if (hours < 10) {
      hours = '0' + hours;
    }
    if (minutes < 10) {
      minutes = '0' + minutes;
    }

    this.setState({
      date: `${date} ${month} ${year}`, clock: `${hours}:${minutes}`
    });
  }

  render() {
    return (
      <React.Fragment>
        <div className={`modal ${this.state.modalActive ? 'on' : ''}`} style={{backgroundColor: this.state.config.modal.bg_color,
        transitionDuration: this.state.config.modal.second + 's',
        color: this.state.config.modal.color}}>
          {this.state.lastTask}
        </div>
        <header>
          <div className="header-main">
            <div className="brand-name">
            Azərbaycan Milli Kitabxanası
            </div>
            <div className="time">
              <span className="date">
                {this.state.date}
              </span>
              <span className="clock" style={{display: this.state.config.clock.display === 'on' ? 'initial' : 'none'}}>
                {this.state.clock}
              </span>
            </div>
          </div>
        </header>
        <section className="main">
          <div className="books">
            <div className="active">
              <ul>
                {this.state.data.map(item => (
                  <li className={`book ${item.STATUS === '0' ? 'failure' : item.STATUS === '1' ? 'pending' : 'success' }`} key={item.OXUCU_ID + item.STATUS}>
                    <style>{`
                      .active .book.${item.STATUS === '0' ? 'failure' : item.STATUS === '1' ? 'pending' : 'success' } .book-id:after {
                        border-left: 27px solid ${this.state.config.tasksConfig[item.STATUS].bg_color};
                      }
                      ${item.STATUS === '0' ? `.active .book.failure .book-id:before {     border-right: 27px solid ${this.state.config.tasksConfig[item.STATUS].bg_color}; }` : ''}
                    `}</style>
                    <div className="book-id" style={{backgroundColor: this.state.config.tasksConfig[item.STATUS].bg_color}}>{item.OXUCU_ID}</div>
                    <div className="book-status" style={{color: this.state.config.tasksConfig[item.STATUS].font_color,
                    fontSize: this.state.config.tasksConfig[item.STATUS].font_size + 'px'}}>{this.state.codes[item.STATUS]}</div>
                  </li>
                ))}
              </ul>
            </div>
            <div className="history">
                <div className="books-success">
                  <ul>
                    {this.state.fulfilled.map((item, index) => (
                      <li key={index} className={typeof item.STATUS === 'string' ? item.STATUS === '3' ? 'done' : 'failure' : null}>{item.OXUCU_ID ? item.OXUCU_ID : item}</li>
                    ))}
                  </ul>
                  <ul>
                    {this.state.rejected.map((item, index) => (
                      <li key={index} className={typeof item.STATUS === 'string' ? item.STATUS === '3' ? 'done' : 'failure' : null}>{item.OXUCU_ID ? item.OXUCU_ID : item}</li>
                    ))}
                  </ul>
                </div>
            </div>
          </div>
        </section>
        <footer>
          <div className="marquee">
            <span style={{animationDuration: `${this.state.config.bottom_config.speed}s`}}>{this.state.config.bottom_config.text}</span>
          </div>
        </footer>
      </React.Fragment>
    );
  }
}

export default App;