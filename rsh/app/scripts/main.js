(function () {
    $(document).ready(function () {
        function check(obj) {
            return obj.length > 0;
        }
        $('*[data-selection=\'none\']').on('mousedown', function (e) {
            e.preventDefault();
            return false;
        });
        if (check($('.placeholder'))) {
            $('.placeholder').css('height', $('#header').innerHeight());
        }
        if (check($('#singleCarousel'))) {
            $('#singleCarousel').owlCarousel({
                items:4,
                mouseDrag: false,
                loop:true,
                responsive: {
                    0: {
                        dots:false,
                        items:2
                    },
                    827: {
                        dots:false,
                        items:4
                    }

                }
            });
            $('.c-right').click(function (e) {
                $('#singleCarousel').trigger('next.owl.carousel');
                e.preventDefault();
                return false;
            });
            $('.c-left').click(function (e) {
                $('#singleCarousel').trigger('prev.owl.carousel');
            });
        }

        if (check($('#brand-carousel'))) {
            $('#brand-carousel').owlCarousel({
                nav: true,
                loop: true,
                dots: false,
                items: 7,
                responsive: {
                    0: {
                        nav: false,
                        items: 2
                    },
                    450: {
                        items: 3,
                        nav: false
                    },
                    520: {
                        items: 4,
                        nav: false
                    },
                    790: {
                        items: 7
                    }
                },
                navText: ['<i class=\'fa fa-arrow-circle-o-left\'></i>',
                    '<i class=\'fa fa-arrow-circle-o-right\'></i>'
                ]
            });
        }
        if (check($('#js-home-carousel'))) {
            var homeCarousel = $('#js-home-carousel');
            homeCarousel.owlCarousel({
                items: 1,
                mouseDrag: false,
                autoplay: true,
                dots: false,
                nav: false,
                navText: [
                    "<i class='fa fa-chevron-left'></i>",
                    "<i class='fa fa-chevron-right'></i>"
                ]
            });
            $('#welcome .next').click(function (e) {
                homeCarousel.trigger('next.owl.carousel');
            });
            $('#welcome .prev').click(function (e) {
                homeCarousel.trigger('prev.owl.carousel');
            });
            homeCarousel.on("changed.owl.carousel", function (e) {
                e.item.index === 0 ? $("#welcome .prev").addClass("disabled") : $("#welcome .prev").removeClass("disabled");
                e.item.index === (e.item.count - 1) ? $("#welcome .next").addClass("disabled") : $("#welcome .next").removeClass("disabled");
            });

        }
        if (check($('.burger'))) {
            $('.nav').css('max-height', $('.nav')[0].scrollHeight);
            $('.burger').click(function () {
                if ($('.js-hide').attr('checked')) {
                    $('.js-hide').attr('checked', false);
                } else {
                    $('.js-hide').attr('checked', true);
                }
                $(this).find('.fa').toggleClass('fa-times');
                $(this).find('.fa').toggleClass('fa-bars');
            });
            window.onresize = function () {
                $('.nav').css('max-height', $('.nav')[0].scrollHeight);
            }
            window.onscroll = function () {
                if ($('.js-hide').attr('checked') === undefined) {
                    $('.js-hide').attr('checked', true);
                    $('.burger').find('.fa').removeClass('fa-times');
                    $('.burger').find('.fa').addClass('fa-bars');
                }
            }
        }
    });

})();