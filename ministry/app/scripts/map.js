$(document).ready(function() {
  const $title = $('.map-tooltip');

  $('.main-map svg path[countryid]').hover(
    e => {
      // console.log($(e.target).attr('countryid'));
      $title.html($(e.target).attr('aria-label'));
      $title.css({
        display: 'block'
      });
    },
    () => {
      $title.css({
        display: 'none'
      });
    }
  );

  $(document).click(e => {
    const target = $(e.target);

    if (!target.closest('[data-city]').length && $(e.target).attr('countryid') === target.attr('data-city')) {
      $('[data-city]').css({
        display: 'none'
      });
    }
  });

  $('.main-map svg path[countryid]').click(e => {
    const target = $('[data-city="' + $(e.target).attr('countryid') + '"]');

    $('[data-city]').css({
      display: 'none'
    });

    target.css({
      display: 'block'
    });
    target.css({
      top: e.pageY - target.outerHeight() / 2,
      left: e.pageX - target.outerWidth() - 50
    });
  });

  $('.main-map').on('mousemove', e => {
    $title.css({
      left: e.pageX + 20,
      top: e.pageY + 10
    });
  });
});
