$(document).ready(() => {
  // toggle header menu.
  $('#ledger-toggle').click(function() {
    $(this)
      .parent()
      .toggleClass('show');
  });

  // click outside.
  $(document).click(e => {
    const isClickInside = $(e.target).closest('.main-header--expand');
    if ($(e.target).closest('#ledger-toggle').length) return;
    if (isClickInside.length === 0) {
      $('#ledger-toggle')
        .parent()
        .removeClass('show');
    }
  });

  $('.js-sub-menu').click(function(e) {
    e.preventDefault();
    $('.js-sub-menu').removeClass('active');
    $(this).addClass('active');
    $(this)
      .closest('.main-header--expand')
      .addClass('expand');
    $('.expand-reveal--item').css({
      display: 'none'
    });
    $(`#${$(this).attr('data-sub-menu')}`).css({
      display: 'block'
    });
  });
  $('.js-sub-menu--back').click(function() {
    $('.js-sub-menu').removeClass('active');
    $(this)
      .closest('.main-header--expand')
      .removeClass('expand');
  });

  function initializeSelectric() {
    const selectrics = $('[data-selectric]');

    selectrics.selectric().on('change', function() {
      persist(this);
    });

    selectrics.each(function(idx, element) {
      persist(element);
    });
  }
  function initializeFaq() {
    const selectrics = $('[data-select-faq]');
    selectrics.selectric().on('change', function() {
      persistFaq(this);
    });
    selectrics.each(function(idx, element) {
      persistFaq(element);
    });
  }
  function persistFaq() {
    const val = $('[data-select-faq]')
      .find(':selected')
      .val();

    $('[data-target]').addClass('d-none');
    $('[data-target="' + val + '"]').removeClass('d-none');
  }

  function persist(element) {
    const selected = $(element).find(':selected'),
      when = selected.attr('data-when'),
      where = selected.attr('data-where'),
      day = selected.attr('data-day'),
      target = $('#' + $(element).attr('data-selectric'));
    target.find('.contact-list--day').html(day === undefined ? '' : day);
    target.find('.contact-list--when').html(when === undefined ? '' : when);
    target.find('.contact-list--where').html(where === undefined ? '' : where);
  }
  initializeFaq();
  initializeSelectric();

  $('[data-custom-masonry]').masonry({
    itemSelector: '.social-news--cards--card',
    columnWidth: '.social-news--cards--sizer',
    percentPosition: true,
    gutter: 20
  });

  $('.js-expand-search-list').click(function(e) {
    $(this)
      .closest('.expand')
      .toggleClass('show');
  });

  let checkboxDebouncer;

  $('.js-checkbox-throttle label').click(function(e) {
    if ($(this).hasClass('loading')) {
      e.preventDefault();
      return false;
    }
    const form = $(this).closest('form');
    $(this).addClass('loading');
    if (checkboxDebouncer) {
      clearTimeout(checkboxDebouncer);
    }
    checkboxDebouncer = setTimeout(() => {
      $('.loading').removeClass('loading');
      form.submit();
    }, 3000);
  });

  $('.main-header--burger').click(e => {
    $('#burger-expand').toggleClass('show');
  });
  // click outside.
  $(document).click(e => {
    const isClickInside = $(e.target).closest('#burger-expand');
    if ($(e.target).closest('.main-header--burger').length) return;
    if (isClickInside.length === 0) {
      $('#burger-expand').removeClass('show');
    }
  });
});
