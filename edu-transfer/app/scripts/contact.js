$(document.body).ready(function() {
    const email = $('#email');
    email.inputmask('email');
    $('#email-confirm').on('keyup', function() {
        if (email.val() !== $(this).val()) {
            $(this).addClass('is-invalid');
        } else {
            $(this).removeClass('is-invalid');
        }
    });
});
