$(document.body).ready(function() {
    const password = $('#password');
    $('#password-confirm').on('keyup', function() {
        if (password.val() !== $(this).val()) {
            $(this).addClass('is-invalid');
        } else {
            $(this).removeClass('is-invalid');
        }
    });
});
