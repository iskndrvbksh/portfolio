$(document.body).ready(function() {
    $.fn.datepicker.language['en'] = {
        days: ['Bazar', 'Bazar Ertəsi', 'Çərşənbə Axşamı', 'Çərşənbə', 'Cümə Axşamı', 'Cümə', 'Şənbə'],
        daysShort: ['Baz', 'Ber', 'Çax', 'Çər', 'Cax', 'Cüm', 'Şnb'],
        daysMin: ['Ba', 'Be', 'Ça', 'Çə', 'Ca', 'Cü', 'Şn'],
        months: ['Yanvar', 'Fevral', 'Mart', 'Aprel', 'May', 'İyun', 'İyul', 'Avqust', 'Sentyabr', 'Oktyabr', 'Noyabr', 'Dekabr'],
        monthsShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
        today: 'Bu Gün',
        clear: 'Təmizlə',
        // set format here
        dateFormat: 'yyyy/dd/mm',
        timeFormat: 'hh:ii aa',
        firstDay: 0
    };
    $('[data-pick-date]').datepicker({
        language: 'en'
    });
});
