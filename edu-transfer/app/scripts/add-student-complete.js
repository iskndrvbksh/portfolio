$(document).ready(() => {
    const langs = $('#foreign-langs');
    $('.js-add-lang').click((e) => {
        const newInput = $(
            `<input
                type="text"
                class="form-control mb-1 w-75 d-inline-block align-middle"
                value="${langs.val()}"
                name="foreign-langs[]"/>`
        );

        langs.val(null);
        langs.before(newInput);
    });
});