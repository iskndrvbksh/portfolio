$('[data-fancybox]').fancybox({
    thumbs:{
        autoStart: true
    },
    buttons: [
        'close',
        'thumbs'
    ]
});