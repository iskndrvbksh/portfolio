(function () {
    var mainCarousel;
    function check(arg) {
        return arg.length > 0;
    }

    if (check($('.js-car'))) {
        mainCarousel = $('.js-car').owlCarousel({
            items:1
        });
    }
    if (check($('.burger'))) {
        $('.burger').click(function () {
            $('.nav').toggleClass('active');
        });
    }
    if (check($('.home-page-carousel'))) {
        var prev = $('.home-page-carousel').find('.prev');
        var next = $('.home-page-carousel').find('.next');
        next.click(function (e) {
            e.preventDefault();
            mainCarousel.trigger('next.owl.carousel');
            return false;
        });
        prev.click(function (e) {
            e.preventDefault();
            mainCarousel.trigger('prev.owl.carousel');
            return false;
        });
    }
})();