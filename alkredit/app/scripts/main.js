$(document).ready(() => {
  let md = new MobileDetect(window.navigator.userAgent);
  if (md.mobile()) {
    document.body.classList.add('mobile');
  }

  $('[data-document]').on('change', function (e) {
    let fileName = e.target.files[0].name;
    $('[data-document-'+ this.getAttribute('data-document') +']').html(fileName);
  });
});