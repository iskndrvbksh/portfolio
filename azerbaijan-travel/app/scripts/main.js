$(document).ready(function() {

    //check element for initializing
    function checkElement(element) {
        if (element.length > 0) {
            return true;
        } else {
            return false;
        }
    }

    function changeSlider(element, changeAnother, anotherElement) {
        let selector = changeAnother == true ? anotherElement : $('.' + element.data('carousel'));
        let toward = element.data('toward');
        selector.trigger('' + toward + '.owl.carousel');
    }

    if (checkElement($('.background'))) {
        // $('.background').owlCarousel({
        //     items: 1,
        //     animateOut: 'fadeOut',
        //     animateIn: 'fadeIn',
        //     autoplay: 9000,
        //     loop: true,
        //     mouseDrag: false
        // });
    }
    $('.mouse-scroll').on('click', () => {
        $('html,body').animate({
            scrollTop: $('.background-container').height() + 'px'
        }, 700);
    })

    if (checkElement($('.item-list'))) {
        $('.item-list').owlCarousel({
            items: 2,
            margin: 10,
            responsive: {
                1024: {
                    items: 2,
                },
                300: {
                    items: 1,
                    dots: true
                }
            }
        })
    }

    $('.navs .nav').click(function() {
        changeSlider($(this));
    })

    if (checkElement($('.frame.instagram'))) {
        var item = $('.frame.instagram').find('.item');
        var images_count = item.length;
        var image_width = item.width() + parseInt(item.css('margin-right'));
        $('.frame.instagram').find('.gallery').width(image_width * images_count - parseInt(item.css('margin-right')));
    }


    //selectboxes
    $('.select-box .current[data-role="selectbox"]').click(function() {
        $(this).siblings('.data-list').fadeToggle('fast');
    })
    $('.select-box .data-list li').click(function() {
        var select_box = $(this).closest('.select-box');
        var value = $(this).data('value');
        var content = $(this).text();
        select_box.find('.value').text(content);
        select_box.find('input[type="hidden"]').val(value);
        select_box.find('.data-list').fadeToggle('fast');
    })

    $(document).on('click', function(e) {
        if ($(e.target).closest('.current').length == 0) {
            $('.data-list').fadeOut('fast');
        }
    })

    //initializing datepicker
    if (checkElement($('[data-toggle="datepicker"]'))) {
        $('[data-toggle="datepicker"]').datepicker({
            autoHide: true
        });
        $('[data-toggle="datepicker"]').on('pick.datepicker', function(e) {
            var selected_date = $(e.target).closest('[data-toggle="datepicker"]').datepicker('getDate', true);
            $(e.target).siblings('span.value').text(selected_date);
        });
    };

    $(window).on('resize load', function() {
        if (checkElement($('.js-submenu')) && window.innerWidth > 1160) {
            var data; //index of submenu 
            // $('.js-submenu').hover(function() {
            //     data = $(this).attr('data-visible');
            //     $('.submenu[data-submenu="' + data + '"]').fadeIn();
            // }, () => {})
            $('.submenu, header .links li>a').hover(() => {}, function() {
                $('.submenu[data-submenu="' + data + '"]').fadeOut();
            });
            $('header .links li').not('.js-submenu').hover(() => {}, function() {
                $('.submenu[data-submenu="' + data + '"]').fadeOut();
            });
        }
    })


    if (checkElement($('.list-item'))) {
        $('.list-item').click(function() {
            $(this).children('.desc').slideToggle();
            $(this).find('.fa').toggleClass('fa-caret-down');
            $(this).find('.fa').toggleClass('fa-caret-up');

        });
    }

    if (window.innerWidth > 995) {
        $(window).scroll(() => {
            if (checkElement($('#insider_header'))) {
                if ($(window).scrollTop() > ($('header').offset().top + $('header').height() + 30)) {
                    $('#insider_header').addClass('scaled');
                } else {
                    $('#insider_header').removeClass('scaled');
                }
            }
        })
    }

    $('.menu-mobile').click(function() {
        $(this).toggleClass('active-collapsed');
        $('header .links').slideToggle().toggleClass('active-mobile')
    })

    if (checkElement($('.mobile-search-box'))) {
        $('.mobile-search-box').click(function() {
            $(this).siblings('.forms').slideToggle('slow');
        })
        $(window).on('resize load', function() {
            if (window.innerWidth > 980) {
                $('.mobile-activated .forms').show();
            }
        })
    }

    $('header .submenu-left .title p').click(function() {
        var index = $(this).data('tab-index');
        $(this).siblings().removeClass('active');
        $(this).addClass('active');
        $.when($('.submenu-links').fadeOut()).then(function() {
            $('.submenu-links').hide();
            $('.submenu-links[data-tab="' + index + '"]').fadeIn();
        })
    })

    $('header .sub-mobile>a').click(function() {
        $(this).closest('.sub-mobile').toggleClass('active');
        $(this).siblings('.submenu-mobile').toggleClass('active');
    })
    $('.submenu-mobile .sub-link').click(function() {
        $(this).siblings().removeClass('active');
        $(this).addClass('active');
    })


    $('header .links>ul>li').hover(function() {
        $('header .links>ul>li>a').not($(this).children('a')).css('opacity', '0.6');
        $(this).children('a').css('opacity', 1);
    }, function() {
        $('header .links>ul>li>a').css('opacity', 1);
    })

    if (checkElement($('.content-links'))) {
        let offset_top;
        // if (window.innerWidth > 1024) {
        //     $(window).scroll(function() {
        //         let window_scroll = $(window).scrollTop();
        //         if (window_scroll > $('#content').offset().top) {
        //             offset_top = $('#content').offset().top;
        //         }
        //         if (window_scroll > (offset_top - 40)) {
        //             $('.content-links>ul').addClass('scrolling');
        //         } else {
        //             $('ul.scrolling').removeClass('scrolling');
        //         }
        //         if (window_scroll > offset_top && (($('ul.scrolling').height() + parseInt($('ul.scrolling').css('margin-top')) + $('ul.scrolling').position().top + 60) < $('#content .content-links').height())) {
        //             let top = window_scroll - offset_top;
        //             $('ul.scrolling').css('top', top + 100);
        //         }
        //     })
        // }
    }

    // if (checkElement($('.content-links'))) {
    //     var initial_ofsset = $('.content-links').offset().top;
    //     console.log(initial_ofsset);
    //     $(window).on('scroll', function() {
    //         if ($(window).scrollTop() >= (initial_ofsset - 40)) {
    //             console.log($(window).scrollTop()>=initial_ofsset);
    //             $('.content-links').addClass('fixed-scroll');
    //         } else {
    //             $('.content-links').removeClass('fixed-scroll');
    //         }
    //     })
    // }
})