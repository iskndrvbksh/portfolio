$('[data-focus]').each((idx, item)=> {
    $(item).click((e)=> {
        e.preventDefault();
        $(item).find('img').attr('src', $(item).attr('data-focus'));
    });
});