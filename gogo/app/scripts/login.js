(function () {
    var links = $('.links');
    var log = links.find('.log');
    var login = $('.login');

    log.click(function (e) {
        e.preventDefault();
        login.toggleClass('active');
        links.toggleClass('active');
        return false;
    });
})();