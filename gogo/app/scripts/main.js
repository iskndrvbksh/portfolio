(function() {
    function checkElement(obj) {
        if (obj.length) return true;
        return false;
    }
    if ($('.num').length > 0) {
        $('.num').click(function(e) {
            var ops = ['plus', 'minus'];
            if (e.target === null || e.target === undefined || !e.target) return;
            var result = $(this).find('.num_result');
            var data = $(e.target).attr('data-behaviour');
            var initialValue = +result.val();
            if (typeof parseInt(initialValue) !== 'number' || isNaN(initialValue)) result.val(0);
            if (ops[0] === data) {
                var d = ++initialValue;
                // d > .num_result::data-max
                result.val(d);
            } else if (ops[1] === data) {
                var d = --initialValue;
                if (d < 0) return;
                result.val(d);
            }
        });
    }
    $(document).click(function(e) {
        if (!$(e.target).closest('[data-propogate=\'true\']').length) {
            $('[data-me=\'1\']').hide();
            $('[data-me=\'2\']').parent().removeClass('active');
            $('[data-me="3"]').removeClass('active');
        }
    });
    if (checkElement($('.header .account > a'))) {
        $('.header .account > a').click(function() {
            $('.account-dropdown').parent().toggleClass('active');
            return false;
        });
    }
    var controls = $('#sideControls');
    var basket = controls.find('.right, .left');
    var close = controls.find('.close');
    var toggler = $('[data-inner]');
    close.click(function() {
        $(document).trigger('click');
    });
    toggler.click(function() {
        $('.' + $(this).attr('data-inner')).toggle();
        return false;
    });
    controls.find('.first .controls > textarea').on('keyup', function() {
        if (parseInt(this.style.height) > 110) {
            return;
        }
        this.style.height = '1px';
        this.style.height = (20 + this.scrollHeight) + 'px';
    });
})();