(function() {
    $(document).ready(function() {
        var owl = $('#car1');
        owl.owlCarousel({
            loop: true,
            dots: false,
            margin: 10,
            responsive: {
                0: {
                    items: 2
                },
                768: {
                    items: 4
                },
                992: {
                    items: 5
                }
            }
        });
        $('.r').click(function() {
            owl.trigger('next.owl.carousel');
        })
        $('.l').click(function() {
            owl.trigger('prev.owl.carousel');
        })
    });
})();