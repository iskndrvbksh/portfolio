function getVals() {
    var parent = $(this).parent();
    var slides = parent.find('input');
    var slide1 = parseFloat(slides[0].value);
    var slide2 = parseFloat(slides[1].value);
    if (slide1 > slide2) {
        var tmp = slide2;
        slide2 = slide1;
        slide1 = tmp;
    }

    var displayElement = parent.find('.rangeValues');
    if (slide1 === slide2) {
        displayElement.html(slide1 + '&#8380;');
        return;
    }
    displayElement.html(+slide1 + '&#8380; - ' + slide2 + '&#8380; ');
}
$(document).ready(function() {
    $('.set > a').on('click', function() {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            $(this).siblings('.content').slideUp(200);
        } else {
            $('.set > a').removeClass('active');
            $(this).addClass('active');
            $('.content').slideUp(200);
            $(this).siblings('.content').slideDown(200);
        }
        return false;
    });
    var sliderSections = $('.range-slider');
    for (var x = 0; x < sliderSections.length; x++) {
        var sliders = sliderSections[x].getElementsByTagName('input');
        for (var y = 0; y < sliders.length; y++) {
            if (sliders[y].type === 'range') {
                sliders[y].oninput = getVals;
                sliders[y].oninput();
            }
        }
    }


    var itemsWrapper = $('.items_wrapper');
    var filterWrapper = $('.filter_wrapper');
    $('.leftPanel').click(function() {
        itemsWrapper.toggleClass('active');
        filterWrapper.toggleClass('active');
    });

    $('.slickOne').owlCarousel({
        items: 2,
        dots:true
        });

    $('[data-pick]').click(function () {
        $(this).closest('.item_logo').toggleClass('activated');
    });
    $('[data-toggle=\'datepicker\']').each(function (item) {
        $(this).datepicker({
            trigger:'[data-pick=\''+item+'\']',
            autoHide: true
        });
    });
    $('[data-recipient]').click(function () {
        $('.recipient').toggleClass('active');
    });
    var timer;
    $('#Carrot .items_wrapper .items .body .item_logo').unbind().hover(function (e) {
        var self = this;
        timer = setTimeout(function () {
            $(self).parent().find('.item_hover').addClass('active');
        }, 500);
    }, function (e) {
            if ($(e.relatedTarget).closest('.recipient').length) {
                return;
            }
            if ($(e.relatedTarget).closest('.datepicker-panel').length) {
                return;
            }
            clearTimeout(timer);
            $(this).parent().find('.item_hover').removeClass('active');
            $(this).parent().find('[data-toggle]').datepicker('hide');
    });
    


    if ($('.replace').length) {
        function closeOverlay() {
            if ($('.image-overlay').length) {
                $('.image-overlay').remove();
                $(document.body).toggleClass('added-overlay');
            }
        }
        var image = $('[data-replace]');
        $(document).on('keydown', function (e) {
            if (e.keyCode === 27) {
                closeOverlay();
            }
        });
        image.click(function () {
            var overlay = $('<div>'),
                img = $('<img>'),
                close = $('<div>');
            
            overlay.addClass('image-overlay');
            img.attr('src', $(this).attr('src'));
            close.unbind().click(function () { closeOverlay(); });
            close.addClass('close-overlay');
            close.append($('<i>').addClass('fa').addClass('fa-close'));

            overlay.append(img);
            overlay.append(close);
            $(document.body).append(overlay);
            $(document.body).addClass('added-overlay');
            return false;
        });
        $('.replace').click(function () {
            var d = $(this).find('img').attr('src');
            image.attr('src', d);
            return false;
        });
    }

    var airport = $('.delivery-items .airport');
    $(document).on('keydown', function (e) {
        if (e.keyCode === 27) {
            $('[data-close-me]').removeClass('active');
        }
    })
    airport.click(function (e) {
        $(this).closest('li').find('.item_hover').addClass('active');
        return false;
    });

});