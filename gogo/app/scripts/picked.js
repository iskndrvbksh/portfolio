$(document).ready(function () {
    var car = $('#pickedCarousel .owl-carousel');
    car.owlCarousel({
        items: 4,
        loop: true,
        margin: 6,
        responsive: {
            0: {
                items: 2
            },
            700: {
                items: 4
            }
        }
    });
});